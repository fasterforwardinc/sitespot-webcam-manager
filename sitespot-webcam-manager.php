<?php
/**
 *
 * @link              https://sitespot.co
 * @package           SiteSpot_Cams
 *
 * Plugin Name:       SiteSpot Cams
 * Plugin URI:        https://sitespot.co
 * Description:       Cache webcams from URL w timelapses.
 * Version:           1.0.3
 * Author:            SiteSpot 
 * Author URI:        https://sitespot.co
 * Text Domain:       sitespot-cams
 * 
 */


add_action('admin_footer', 'take_snapshot_admin_footer_function');
function take_snapshot_admin_footer_function() {
	//take_snapshot();
	// delete_old_snaps();
	//latest_cam('easy-street');
	
}

//updates cams oldest to newest.
function take_snapshot(){

	if(is_night())
		return false;
	
	$now = (int) time();
	$args = array(
		'post_type'   => 'snow_cams',
		'orderby'  => 'meta_value_num',
		'meta_key' => 'next_update',
		'order' => 'ASC', // last updated? 
		'numberposts'      => 1,	
		'meta_query'        => array(
			array(
				'key' => 'next_update',
				'value' => $now,
				'compare' => '<',
				'type' => 'numeric',
			)
		),
	);

	$cams = get_posts($args);

	//mm($cams);

	if(empty($cams)){
		mm('no cams need updating');
		return false;
	}

	foreach($cams as $cam)
	{
		mm('updating cam ' . $cam->post_title);
		$imageUrl =  get_post_meta($cam->ID,'image_url',true);
		$imageTag = sanitize_title( $cam->post_title);
		// if its not the same as the latest

		if(images_equal($imageUrl, latest_cam($imageTag))){			
			//mm('duplicate images, stopping');
			set_next_update($cam->ID,2); // check in 2 minutes
			return false;
		}
		else
		{
		}

		// create post
		$id = wp_insert_post(array(
			'post_title'=>$imageTag."-".time(), 
			'post_type'=>'snapshot',     
			'post_status'  => 'publish',
			'post_content'=> $cam->post_title,
			'tags_input' => array($imageTag),
		));

		if($id)
		{
			Generate_Featured_Image($imageUrl, $id);
		}
		// update the post so it moves to the end of the queue
		set_next_update($cam->ID,30);

		//finito
		//mm('fin');
	}
}


function set_next_update($id, $minutes){
	mm("$id next update in $minutes mins");
	$updateTime = time() + ($minutes * 60);
	update_post_meta($id,'next_update',$updateTime);
}

function is_night(){
	$now = time(); // unix gmt
	$sun_info = date_sun_info ( time() , -43.53, 172.6 );
	$sunrise = $sun_info['civil_twilight_begin'];
	$sunset = $sun_info['civil_twilight_end'];

	return ($now < $sunrise) || ($now > $sunset);
}



//delete cams older than 24 hours
function delete_old_snaps(){

	$args = array(
		'post_type'   => 'snapshot',
		'numberposts' => 10, // max 10 to avoid server loads
		'date_query' => array(
			'before' => date('Y-m-d', strtotime('-1 day')) 
		)
	);

	$cams = get_posts($args);

	if($cams){
		foreach($cams as $cam){
			wp_delete_post($cam->ID,true);
		}
	}
}

// remove featured image when deleting an old timelapse

//todo check post type

add_action( 'before_delete_post', 'wps_remove_attachment_with_post', 10 );
function wps_remove_attachment_with_post($post_id)
{
    if(has_post_thumbnail( $post_id ))
	{
      	$attachment_id = get_post_thumbnail_id( $post_id );
    	$deleted = wp_delete_attachment($attachment_id, true);
    }
}

function Generate_Featured_Image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = "cam-".time()."-".basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))
      $file = $upload_dir['path'] . '/' . $filename;
    else
      $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}


// function that runs when shortcode is called
function all_snapshots($atts, $content = null) {
    $a = shortcode_atts( array(
        'cam' => 'cam',
    ), $atts );

	$tag = $a['cam']; // todo dynamic in shortcode

 	$args = array(
		'post_type'   => 'snapshot',
		'numberposts' => -1, // all? result
		'tag' => $tag, 
	);

	$shots = get_posts($args);

	if($shots){
		
		// Things that you want to do. 
		$html = ""; // return contents for slick slider
		$images = [];

		foreach($shots as $shot){
			$html .= "<div><img src='".wp_get_attachment_url( get_post_thumbnail_id($shot->ID,'medium') )."' data-stamp='".$shot->ID."'></div>";
		}
		
		return $html;

	}

	return "no shots :/";
} 
// register shortcodeall_snapshots
add_shortcode('all_snapshots', 'all_snapshots'); 


// get the latest snapshot image URL for a tag
function latest_cam($tag){

 	$args = array(
		'post_type'   => 'snapshot',
		'numberposts' => 1, // all? result
		'tag' => $tag, 
	);

	$shots = get_posts($args);

	return wp_get_attachment_url( get_post_thumbnail_id($shots[0]->ID,'full') );

}


function images_equal($image1, $image2){

	return md5_file($image1) === md5_file($image2);

}
























if(!function_exists('mm'))
{

	function mm($data){

		echo "<pre style='margin-left:100px;'>";
		print_r($data);
		echo "</pre>";

		wp_mail('tom@sitespot.co','debug',print_r($data,true));

	}

}